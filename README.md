# Projects

The below upstream repositories of the projects are added as submodules [here](https://gitlab.com/indicproject/Projects/tree/master). Contributions
and development must happen in those repositories.

* Libindic https://github.com/libindic
* Indic Keyboard https://gitlab.com/smc/indic-keyboard
* Varnam Project https://github.com/varnamproject
* Libvarnam https://github.com/varnamproject/libvarnam
* Libindic SDK https://github.com/libindic/libindic-sdk
* Dhavani TTS https://github.com/dhvani-tts/dhvani-tts
* Ibus-braille https://gitlab.com/smc/ibus-braille
* Indian language Hyphenations https://gitlab.com/smc/hyphenation
* Indic Font Book https://github.com/IndicFontbook/Fontbook
* Indicjs https://github.com/indicjs
* Grandham - Indic Biblio Opendata portal https://gitlab.com/smc/grandham
* Automated Rendering Testing https://gitlab.com/smc/automated-rendering-testing


## GSoC Projects from 2016

Below are the projects that were done last year, and have not yet been merged to the
upstream sources as they are ongoing.

* Integrating Varnam to Indic Keyboard -
  https://github.com/malayaleecoder/Indic-Keyboard
* Speech Recognition for Malayalam -
  https://github.com/sreecodeslayer/ml-am-lm-cmusphinx
* Indic Keyboard user onboarding, additional layouts -
  https://github.com/arushidogra/Indic-Keyboard
